import React from 'react';
import BottomTabNav from './src/navigation/Navigation';

const App = () => {
  return (
    <>
      <BottomTabNav />
    </>
  );
};

export default App;
