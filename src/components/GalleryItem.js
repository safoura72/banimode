import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';
import {RFValue} from 'react-native-responsive-fontsize';

export const GalleryItem = ({imageUri, price}) => {
  return (
    <View style={styles.container}>
      <View style={styles.image_content}>
        <View style={styles.first_row}>
          <View style={styles.off}>
            <Text style={styles.off_value}>% ۱۵</Text>
          </View>
          <Image
            style={styles.exit}
            source={require('../assets/icons/exit.png')}
          />
        </View>
        <Image
          style={styles.img}
          source={require('../assets/images/shoes.jpg')}
        />
        <View style={styles.buy_goods_icon_content}>
          <Image
            style={styles.buy_goods_icon}
            source={require('../assets/icons/add.png')}
          />
        </View>
      </View>

      <View style={styles.detail_content}>
        <Text style={styles.detail_text}>NIKE Shoe</Text>
        <Text style={styles.detail_text}>کفش نایکی</Text>
        <Text style={styles.detail_text}>{price}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '35%',
    height: responsiveWidth(70),
    marginLeft: responsiveWidth(2),
    borderRadius: responsiveWidth(4),
  },
  img: {
    width: '100%',
    height: '40%',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: responsiveHeight(2),
  },
  image_content: {
    height: responsiveHeight(23),
    borderRadius: responsiveWidth(3),
    backgroundColor: '#f5f5f5',
  },
  off: {
    width: responsiveWidth(9.5),
    height: responsiveHeight(3.25),
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    backgroundColor: '#ff471a',
  },
  first_row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: responsiveHeight(2),
  },
  off_value: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: RFValue(14),
    paddingVertical: 2,
    paddingHorizontal: 3,
  },
  exit: {
    width: responsiveWidth(6),
    height: responsiveWidth(6),
    marginHorizontal: responsiveWidth(2.5),
  },
  buy_goods_icon_content: {
    flexDirection: 'row',
    marginLeft: 5,
    borderRadius: 25,
    width: responsiveWidth(6),
    height: responsiveWidth(6),
    marginVertical: 7,
    backgroundColor: 'white',
  },
  buy_goods_icon: {
    alignSelf: 'center',
    marginHorizontal: 6,
    width: responsiveWidth(4),
    height: responsiveWidth(4),
  },
  detail_content: {
    marginVertical: 4,
  },
  detail_text: {
    textAlign: 'right',
    paddingTop: 5,
    fontSize: RFValue(12),
    color: 'black',
    fontFamily: 'IRANSans',
  },
});
