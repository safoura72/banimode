import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';
import {RFValue} from 'react-native-responsive-fontsize';

export const HeaderLayout = ({title, children}) => {
  return (
    <View style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <View style={styles.titles}>
          <Text numberOfLines={1} style={styles.text}>
            {title}
          </Text>
        </View>
      </SafeAreaView>
      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '8%',
    width: '100%',
    borderWidth: 0.5,
    paddingTop: 17,
    alignItems: 'center',
    alignSelf: 'center',
  },
  titles: {
    width: responsiveWidth(72.4),
    alignSelf: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  text: {
    color: 'black',
    fontSize: RFValue(17),
    fontFamily: 'IRANSans',
    textAlign: 'center',
    fontWeight: 'bold',
  },
});
