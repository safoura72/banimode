import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {HeaderLayout} from '../components';

export const SearchScreen = () => {
  return (
    <HeaderLayout showIcon={true} title={'لیست علاقه مندی ها'}>
      <View style={styles.container}>
        <Text> جستجو</Text>
      </View>
    </HeaderLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
