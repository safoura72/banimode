import React, {useState, useEffect} from 'react';
import {HeaderLayout, GalleryList, GalleryItem} from '../components';
import {StyleSheet, View, ScrollView} from 'react-native';

export const GalleryScreen = ({navigation}) => {
  const [data, setData] = useState([]);
  const [visible, setVisible] = useState(false);
  const [state, setState] = useState({
    loading: false,
    refreshing: false,
    page: 0,
  });

  return (
    <HeaderLayout showIcon={true} title={'لیست علاقه مندی ها'}>
      {/* <GalleryList
        // onPress={(item) => navigation.navigate('GalleryDetail', {item})}
        // data={fetchTaking}
        // onEndReached={moreScrollFetch}
        // onEndReachedThreshold={0}
        // onRefresh={getAlbums}
        // refreshing={state.loading}
        // onEndReach={callNext}
      /> */}
      <ScrollView style={styles.container}>
        <View style={styles.content}>
          <GalleryItem price={'۸,۲۵۰,۰۰۰'} />
          <GalleryItem price={'۷,۷۵۰,۰۰۰'} />
        </View>
        <View style={styles.content}>
          <GalleryItem price={'۵,۵۰۰,۰۰۰'} />
          <GalleryItem price={'۴,۲۵۰,۰۰۰'} />
        </View>
        <View style={styles.content}>
          <GalleryItem price={'۲۲,۰۰۰,۰۰۰'} />
          <GalleryItem price={'۸,۲۵۰,۰۰۰'} />
        </View>
      </ScrollView>
    </HeaderLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    backgroundColor: 'white',
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
