import React from 'react';
import {createAppContainer} from 'react-navigation';
import {Image} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {
  BasketScreen,
  GalleryScreen,
  FavoriteScreen,
  ProfileScreen,
  SearchScreen,
} from '../screens';

const BottomTabNav = createBottomTabNavigator(
  {
    Profile: {
      screen: ProfileScreen,
      navigationOptions: () => ({
        tabBarIcon: () => (
          <Image source={require('../assets/icons/profile.png')} />
        ),
      }),
    },

    Favorite: {
      screen: FavoriteScreen,
      navigationOptions: () => ({
        tabBarIcon: () => (
          <Image source={require('../assets/icons/favorite.png')} />
        ),
      }),
    },
    Gallery: {
      screen: GalleryScreen,
      navigationOptions: ({navigation}) => ({
        tabBarIcon: () => (
          <Image source={require('../assets/icons/Gallery.png')} />
        ),
      }),
    },
    Search: {
      screen: SearchScreen,
      navigationOptions: () => ({
        tabBarIcon: () => (
          <Image source={require('../assets/icons/search.png')} />
        ),
      }),
    },
    Basket: {
      screen: BasketScreen,
      navigationOptions: () => ({
        tabBarIcon: () => (
          <Image source={require('../assets/icons/basket.png')} />
        ),
      }),
    },
  },
  {
    tabBarOptions: {
      showLabel: false,
    },
    initialRouteName: 'Gallery',
  },
);

export default createAppContainer(BottomTabNav);
